# argus-onboarding-uc

## Description

This project implements the
[Clean Code Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)
Application Business Rules" layer (aka Use Cases) for the onboarding feature.  The project is responsible for
development of onboarding use cases.

This project contains Argus artifacts supporting use cases (aka interactors). It exists to separate use case
concerns and support independent developability and deployment.

This project defines Argus interfaces in the Clean Architecture use case layer for the Onboarding feature.
It exists to specify the business logic used by the Main and User Interface layers.

## License

GPL, Version 3.0.  See the peer document LICENSE for details.

## Contributions

See the [contributing guide](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/CONTRIBUTING.md) in the
[Argus Documentation Project](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/README.md).

## Project status

The current, released version is 0.9.1

## Documentation

For general documentation on Argus, see the
[Argus Documentation Project](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/README.md).

As documentation entered into code files grows stale seconds after it is written, no such documentation is created.
Instead, documentation is created by you on demand using the Dokka Gradle task: 'dokkaGfm'. After successful task
completion, see the detailed documentation [here](build/dokka/gfm/index.md)

## Usage

To use the project, follow these steps:

1. Add the project as a dependency in your build file.
2. Import the necessary classes and interfaces from the project.
3. Use the provided APIs to interact with the shelf feature.

## Test Cases

### Overview

The table below identifies the adapter layer unit tests. A test file name is always of the form `NamePrefixUnitTest.kt`.
The test file content is one or more test cases (functions)

| Filename Prefix | Test Case Name                                                                               |
|-----------------|----------------------------------------------------------------------------------------------|
| GetNetworksList | When accessing the default (null) onboarding network list, verify an onboarding error        |
|                 | When accessing the onboarding network list before doing onboarding, verify a premature error |
|                 | When accessing a valid onboarding network list, verify the list size                         |
| GetProfilesList | When accessing the default (null) onboarding profile list, verify an onboarding error        |
|                 | When accessing the onboarding profile list before doing onboarding, verify a premature error |
|                 | When accessing a valid onboarding profile list, verify the list size                         |
| GetTmdbApiKey   | When accessing the default onboarding TMDb api key, verify an onboarding error is thrown     |
|                 | When accessing the onboarding TMDb api key before doing onboarding, verify a premature error |
|                 | When accessing the default onboarding TMDb api key, verify an inject error is thrown         |
| NeedsOnboarding | When the default onboarding value is null, verify an inject error is thrown                  |
|                 | When the onboarding hasOnboarded property is false, verify onboarding is needed              |
|                 | When the onboarding hasOnboarded property is true, verify onboarding is not needed           |

### Notes

The single responsibility for this project is to provide the implementation of onboarding use sases for Argus. In the Clean
Architecture onion diagram, this project corresponds to the Use Case layer.

The Argus onboarding feature use cases are:

- tbd

The single responsibility for this project is to implement the business logic for the Onboarding feature.

The core layer provides interfaces expected by the onboarding business logic.

The adapter layer provides the business implementation of the core interfaces used in the use case layer.
