package com.pajato.argus.onboarding.uc

import com.pajato.argus.network.core.Network
import com.pajato.argus.onboarding.core.OnboardingError
import com.pajato.argus.onboarding.core.OnboardingRepo
import com.pajato.argus.onboarding.uc.OnboardingProperty.API_KEY
import com.pajato.argus.onboarding.uc.OnboardingProperty.NETWORKS
import com.pajato.argus.onboarding.uc.OnboardingProperty.PROFILES
import com.pajato.argus.profile.core.Profile
import kotlinx.serialization.json.Json

internal const val NULL_INJECT_ERROR = "No onboarding URI value has been injected!"
internal const val PREMATURE_ERROR = "Cannot be called before onboarding has been run!"

private enum class OnboardingProperty { API_KEY, NETWORKS, PROFILES }

/**
 * Checks if the user needs to go through the onboarding process.
 *
 * @param repo The OnboardingRepo object used to retrieve the user's onboarding information.
 * @return True if the user needs onboarding, False otherwise.
 * @throws OnboardingError if the provided OnboardingRepo object is null
 */
fun needsOnboarding(repo: OnboardingRepo): Boolean {
    val onboarding = repo.onboarding ?: throw OnboardingError(NULL_INJECT_ERROR)
    return !onboarding.hasOnboarded
}

private fun validate(repo: OnboardingRepo, key: OnboardingProperty): String {
    val onboarding = repo.onboarding ?: throw OnboardingError(NULL_INJECT_ERROR)

    fun valueOf(key: OnboardingProperty): String = when (key) {
        API_KEY -> onboarding.tmdbApiKey
        NETWORKS -> onboarding.networkListAsJson
        PROFILES -> onboarding.profileListAsJson
    }

    if (!onboarding.hasOnboarded) throw OnboardingError(PREMATURE_ERROR)
    return valueOf(key)
}

/**
 * Returns the TMDb (The Movie Database) API key from the given OnboardingRepo object.
 *
 * @param repo The OnboardingRepo object to retrieve the TMDB API key from.
 * @return The TMDB API key as a string.
 */
fun getTmdbApiKey(repo: OnboardingRepo): String = validate(repo, API_KEY)

/**
 * Retrieves a list of profiles from the provided OnboardingRepo.
 *
 * @param repo the OnboardingRepo to fetch the profiles from
 * @return a list of profiles retrieved from the OnboardingRepo
 */
fun getProfileList(repo: OnboardingRepo): List<Profile> = validate(repo, PROFILES).let { Json.decodeFromString(it) }

/**
 * Retrieves the list of networks from the given OnboardingRepo.
 *
 * @param repo The OnboardingRepo object to retrieve the networks from.
 * @return The list of Network objects retrieved from the OnboardingRepo.
 */
fun getNetworkList(repo: OnboardingRepo): List<Network> = validate(repo, NETWORKS).let { Json.decodeFromString(it) }
