package com.pajato.argus.onboarding.uc

import com.pajato.argus.onboarding.core.Onboarding
import com.pajato.argus.onboarding.core.OnboardingError
import com.pajato.argus.onboarding.core.OnboardingRepo
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File
import java.net.URI
import kotlin.io.path.toPath

object TestRepo : OnboardingRepo {
    private var uri: URI? = null
    override var onboarding: Onboarding? = null

    override suspend fun inject(uri: URI?) {
        this.uri = uri
        onboarding = if (uri != null) Json.decodeFromString(uri.toPath().toFile().readText()) else null
    }

    override suspend fun persist() {
        val uri = this.uri ?: throw OnboardingError(NULL_INJECT_ERROR)
        val file: File = uri.toPath().toFile()

        fun save(file: File, json: String) { file.writeText(json) }

        fun delete(file: File) { file.delete() }

        if (onboarding != null) save(file, Json.encodeToString(onboarding)) else delete(file)
    }
}
