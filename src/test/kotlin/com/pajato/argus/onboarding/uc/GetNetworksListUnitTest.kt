package com.pajato.argus.onboarding.uc

import com.pajato.argus.onboarding.core.OnboardingError
import com.pajato.test.ReportingTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class GetNetworksListUnitTest : ReportingTestProfiler() {

    @Test fun `When accessing the default (null) onboarding network list, verify an onboarding error`() {
        TestRepo.onboarding = null
        assertFailsWith<OnboardingError> { getNetworkList(TestRepo) }.also {
            assertEquals(NULL_INJECT_ERROR, it.message)
        }
    }

    @Test fun `When accessing the onboarding network list before doing onboarding, verify a premature error`() {
        TestRepo.onboarding = TestAgent.onboarding
        assertFailsWith<OnboardingError> { getNetworkList(TestRepo) }.also { assertEquals(PREMATURE_ERROR, it.message) }
    }

    @Test fun `When accessing a valid onboarding network list, verify the list size`() {
        val onboarding = TestAgent.onboarding.copy(hasOnboarded = true)
        TestRepo.onboarding = onboarding
        assertEquals(2, getNetworkList(TestRepo).size)
    }
}
