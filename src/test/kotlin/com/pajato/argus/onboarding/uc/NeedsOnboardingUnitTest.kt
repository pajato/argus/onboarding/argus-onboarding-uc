package com.pajato.argus.onboarding.uc

import com.pajato.argus.onboarding.core.OnboardingError
import com.pajato.test.ReportingTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class NeedsOnboardingUnitTest : ReportingTestProfiler() {

    @Test fun `When the default onboarding value is null, verify an inject error is thrown`() {
        TestRepo.onboarding = null
        assertFailsWith<OnboardingError> { needsOnboarding(TestRepo) }.also { assertEquals(NULL_INJECT_ERROR, it.message) }
    }

    @Test fun `When the onboarding hasOnboarded property is false, verify onboarding is needed`() {
        TestRepo.onboarding = TestAgent.onboarding
        assertTrue(needsOnboarding(TestRepo))
    }

    @Test fun `When the onboarding hasOnboarded property is true, verify onboarding is not needed`() {
        TestRepo.onboarding = TestAgent.onboarding.copy(hasOnboarded = true)
        assertFalse(needsOnboarding(TestRepo))
    }
}
