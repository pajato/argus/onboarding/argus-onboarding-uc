package com.pajato.argus.onboarding.uc

import com.pajato.argus.onboarding.core.OnboardingError
import com.pajato.test.ReportingTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class GetTmdbApiKeyUnitTest : ReportingTestProfiler() {

    @Test fun `When accessing the default onboarding TMDb api key, verify an onboarding error is thrown`() {
        TestRepo.onboarding = null
        assertFailsWith<OnboardingError> { getTmdbApiKey(TestRepo) }.also {
            assertEquals(NULL_INJECT_ERROR, it.message)
        }
    }

    @Test fun `When accessing the onboarding TMDb api key before doing onboarding, verify a premature error`() {
        TestRepo.onboarding = TestAgent.onboarding
        assertFailsWith<OnboardingError> { getTmdbApiKey(TestRepo) }.also { assertEquals(PREMATURE_ERROR, it.message) }
    }

    @Test fun `When accessing the default onboarding TMDb api key, verify an inject error is thrown`() {
        val onboarding = TestAgent.onboarding.copy(hasOnboarded = true)
        TestRepo.onboarding = onboarding
        assertEquals(TestAgent.tmdbApiKey, getTmdbApiKey(TestRepo))
    }
}
