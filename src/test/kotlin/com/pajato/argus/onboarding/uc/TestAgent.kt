package com.pajato.argus.onboarding.uc

import com.pajato.argus.onboarding.core.Onboarding

object TestAgent {
    val hasOnboarded = false
    val tmdbApiKey = "xyzzy"
    val networksJson = """[{"id":23},{"id":6}]"""
    val profilesJson = """[{"id":100},{"id":101},{"id":102}]"""
    val onboarding = Onboarding(hasOnboarded, tmdbApiKey, networksJson, profilesJson)
}
