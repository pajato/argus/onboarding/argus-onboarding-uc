package com.pajato.argus.onboarding.uc

import com.pajato.argus.onboarding.core.OnboardingError
import com.pajato.test.ReportingTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class GetProfileListUnitTest : ReportingTestProfiler() {

    @Test fun `When accessing the default onboarding profile list, verify an onboarding error`() {
        TestRepo.onboarding = null
        assertFailsWith<OnboardingError> { getProfileList(TestRepo) }.also {
            assertEquals(NULL_INJECT_ERROR, it.message)
        }
    }

    @Test fun `When accessing the onboarding profile list before doing onboarding, verify a premature error`() {
        TestRepo.onboarding = TestAgent.onboarding
        assertFailsWith<OnboardingError> { getProfileList(TestRepo) }.also {
            assertEquals(PREMATURE_ERROR, it.message)
        }
    }

    @Test fun `When accessing the default onboarding profile list, verify the size`() {
        val onboarding = TestAgent.onboarding.copy(hasOnboarded = true)
        TestRepo.onboarding = onboarding
        assertEquals(3, getProfileList(TestRepo).size)
    }
}
